package geekOfGeeks;

import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class SecondSmallestElement {
	static int print2Smallest(int arr[]) {
		int first= Integer.MAX_VALUE, second= Integer.MAX_VALUE;
		int arr_size = arr.length;
        
        /* There should be atleast two elements */
        if (arr_size < 2)
        {
        	System.out.println(" Invalid Input ");
            return -1;
        }
        //IntStream stream1 = Arrays.stream(arr);
         //return stream1.reduce(Integer.MAX_VALUE, (a, b) -> Integer.min(a, b));
        //return stream1.reduce(first, (a, b) -> Integer.min(a, b));
        for(int i=0;i<arr_size;i++) {
        	if(arr[i]<first) {
        		second=first;
        		first=arr[i];
        	}
        	else if (arr[i] < second && arr[i] != first)
                second = arr[i];
        }
        return second;
	}
	
	public static void main (String[] args)
    {
        int arr[] = {12, 13, 1, 10, 34, 1};
        System.out.println("Integer value   :::: "+print2Smallest(arr));
    }
}
