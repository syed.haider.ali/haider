package geekOfGeeks;

public class SecondSmallestElementRotated {
	static int print2Smallest(int arr[]) {
		int diff;
		int peak=-1;
		int arr_size = arr.length;
        
        /* There should be atleast two elements */
        if (arr_size < 2)
        {
        	System.out.println(" Invalid Input ");
            return -1;
        }
        diff=arr[1]-arr[0];
        for(int i=1;i<arr_size-1;i++) {
        	if(diff!=arr[i+1]-arr[i]){
        		peak=i;
        		break;
        	}
        	
        }
        //peak peak-(peak+1)
        System.out.println(peak);
        System.out.println((arr_size+1-2));
        return diff>0?arr[(peak+2)%arr_size]:arr[peak-1];
	}
	
	public static void main (String[] args)
    {
        int arr[] = {1,6,5,4,3,2};
        System.out.println("Integer value   :::: "+print2Smallest(arr));
    }
}
