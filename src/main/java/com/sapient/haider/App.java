package com.sapient.haider;

import java.util.Arrays;
import java.util.stream.Collectors;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;

import com.sapient.haider.entity.ApplicatioUserRepository;
import com.sapient.haider.entity.ApplicationUser;
import com.sapient.haider.entity.Department;
import com.sapient.haider.entity.DepartmentRepository;
import com.sapient.haider.entity.Employee;
import com.sapient.haider.entity.Person;

import com.sapient.haider.service.DataService;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableJpaAuditing
@EnableSwagger2
public class App {
	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}

	@Bean
	InitializingBean sendDatabase(final DataService dataService) {
		return () -> {
			Person person1=new Person("Haider","Ali");
			Person person2=new Person("Aqib","Syed");
			dataService.save(person1);
			dataService.save(person2);
			Employee employee1=new Employee(person1, 1);
			Employee employee2=new Employee(person2, 2);
			dataService.save(employee1);
			dataService.save(employee2);
			Department department=new Department("Testing dep", Arrays.asList(employee1,employee2).stream().collect(Collectors.toSet()));
			dataService.save(department);
		};
	}
	
	@Bean
	InitializingBean updateDatabase(final DepartmentRepository departmentRepository,final ApplicatioUserRepository applicatioUserRepository) {
		return () -> {
			Department department= departmentRepository.findByName("Testing dep");
			department.setName("Final Department");
			departmentRepository.saveAndFlush(department);
			ApplicationUser applicationUser=new ApplicationUser();
			applicationUser.setUsername("haider");
			applicationUser.setPassword(bCryptPasswordEncoder().encode("haider"));
			applicatioUserRepository.save(applicationUser);
		};
	}
	
	@Bean
    public Docket api() { 
        return new Docket(DocumentationType.SWAGGER_2)  
          .select()                                  
          .apis(RequestHandlerSelectors.any())              
          .paths(PathSelectors.any())                          
          .build();                                           
    }
	
	
	@Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
