package com.sapient.haider.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Employee {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private int salary;
	
	
	@OneToOne
	private Person person;

	public Person getPerson() {
		return person;
	}
	
	public Employee() {
	}



	public Employee(Person person, int salary) {
		this.person=person;
		this.salary = salary;
	}

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public int getSalary() {
		return salary;
	}
}
