package com.sapient.haider.entity;


public interface DepartmentRepository extends BaseRepository<Department> {
	Department findByName(String name);
}
