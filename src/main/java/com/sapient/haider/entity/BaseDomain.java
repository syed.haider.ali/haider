package com.sapient.haider.entity;

import java.util.Date;

import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sapient.haider.hiberate.interceptor.LoggigInterceptor;

@MappedSuperclass
@EntityListeners(LoggigInterceptor.class)
@JsonIgnoreProperties(value = {"creationDate", "lastModifiedBy","version","hibernateLazyInitializer", "handler"})
public abstract class BaseDomain {
	@CreatedBy
    protected String createdBy;

    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    protected Date creationDate;

    @LastModifiedBy
    protected String lastModifiedBy;

    @LastModifiedDate
    @Temporal(TemporalType.TIMESTAMP)
    protected Date lastModifiedDate;
    
    @Version
	private int version;
	
	public int getVersion() {
		return version;
	}
}
