package com.sapient.haider.entity;

public interface ApplicatioUserRepository extends BaseRepository<ApplicationUser> {
	ApplicationUser findByUsername(String username);
}
