package com.sapient.haider.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sapient.haider.entity.Department;
import com.sapient.haider.entity.DepartmentRepository;
import com.sapient.haider.entity.Employee;
import com.sapient.haider.entity.EmployeeRepository;
import com.sapient.haider.entity.Person;
import com.sapient.haider.entity.PersonRepository;

@Transactional(propagation= Propagation.REQUIRED, readOnly=false, isolation= Isolation.REPEATABLE_READ)
@Service("dataService")
public class DataService {
	@Autowired
	private DepartmentRepository departmentRepository;
	@Autowired
	private PersonRepository personRepository;
	@Autowired
	private EmployeeRepository employeeRepository;
	
	public Employee save(Employee employee) {
		return employeeRepository.save(employee);
	}
	
	public Person save(Person person) {
		return personRepository.save(person);
	}
	
	public Department save(Department department) {
		return departmentRepository.save(department);
	}
}
