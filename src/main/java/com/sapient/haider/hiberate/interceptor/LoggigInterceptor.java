package com.sapient.haider.hiberate.interceptor;

import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;

public class LoggigInterceptor{
	@PrePersist
    @PreUpdate
    @PreRemove
    private void beforeAnyOperation(Object object) {
		System.out.println("This is object getting printed :"+ object.getClass().getSimpleName());
	}

}
