package com.sapient.haider.controller;

import java.util.List;


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sapient.haider.entity.BaseRepository;
import com.sapient.haider.entity.SearchCriteria;

public class BaseController<T,U extends BaseRepository<T>> {
	@Autowired
	public U repository;
	@PersistenceContext
	private EntityManager entityManager;
	
	final Class<T> typeParameterClass;

	public BaseController(Class<T> typeParameterClass){
		this.typeParameterClass=typeParameterClass;
	}

	@RequestMapping(method=RequestMethod.GET)
	public List<T> get() {
		return repository.findAll();
	};
	
	@RequestMapping(path="/{id}",method=RequestMethod.GET)
	public T get(@PathVariable("id") Long id) {
		return repository.getOne(id);
	}
	
	@RequestMapping(path="/query",method=RequestMethod.POST)
	public List<T> query(@RequestBody List<SearchCriteria> params) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<T> query = builder.createQuery(typeParameterClass);
		Root<T> r = query.from(typeParameterClass);
		Predicate predicate = builder.conjunction();
		for (SearchCriteria param : params) {
            if (param.getOperation().equalsIgnoreCase(">")) {
                predicate = builder.and(predicate, 
                  builder.greaterThanOrEqualTo(r.get(param.getKey()), 
                  param.getValue().toString()));
            } else if (param.getOperation().equalsIgnoreCase("<")) {
                predicate = builder.and(predicate, 
                  builder.lessThanOrEqualTo(r.get(param.getKey()), 
                  param.getValue().toString()));
            } else if (param.getOperation().equalsIgnoreCase(":")) {
                if (r.get(param.getKey()).getJavaType() == String.class) {
                    predicate = builder.and(predicate, 
                      builder.like(r.get(param.getKey()), 
                      "%" + param.getValue() + "%"));
                } else {
                    predicate = builder.and(predicate, 
                      builder.equal(r.get(param.getKey()), param.getValue()));
                }
            }
        }
        query.where(predicate);
 
        List<T> result = entityManager.createQuery(query).getResultList();
        return result;
	}
}
