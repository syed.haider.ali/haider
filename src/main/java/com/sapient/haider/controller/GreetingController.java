package com.sapient.haider.controller;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sapient.haider.entity.Person;
import com.sapient.haider.entity.PersonRepository;

import static com.sapient.haider.java8.ComparatorExample.*;

@RestController
public class GreetingController {
	
	@Autowired
	private PersonRepository personRepository;
	
    @RequestMapping("/greeting")
    public Map greeting(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
        model.addAttribute("name", name);
        Map<String,Object> result=new HashMap<>();
        result.put("status", 200);
        result.put("data", "greetings");
        return result;
    }
    
    @RequestMapping("/comaprator")
    public List comparator(@RequestParam(name="type", required=false, defaultValue="8") String type) {
    	List<Person> persons=personRepository.findAll();
    	switch (type) {
		case "7":
			Collections.sort(persons, byFirstName);
			break;

		default:
			Collections.sort(persons, byLastName);
			break;
		}
        return persons;
    } 

}