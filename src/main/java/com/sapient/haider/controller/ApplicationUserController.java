package com.sapient.haider.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sapient.haider.entity.ApplicatioUserRepository;
import com.sapient.haider.entity.ApplicationUser;

@RequestMapping("/users")
public class ApplicationUserController extends BaseController<ApplicationUser, ApplicatioUserRepository> {

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
    
	public ApplicationUserController() {
		super(ApplicationUser.class);
	}
	
	@PostMapping("/sign-up")
    public void signUp(@RequestBody ApplicationUser user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        repository.save(user);
    }
}
