package com.sapient.haider.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sapient.haider.entity.Person;
import com.sapient.haider.entity.PersonRepository;

@RestController
@RequestMapping("/person")
public class PersonController extends BaseController<Person, PersonRepository> {

	public PersonController() {
		super(Person.class);
	}
	
}
