package com.sapient.haider.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sapient.haider.entity.Department;
import com.sapient.haider.entity.DepartmentRepository;

@RestController
@RequestMapping("/department")
public class DepartmentController extends BaseController<Department, DepartmentRepository> {

	public DepartmentController() {
		super(Department.class);
	}
	
}
