package com.sapient.haider.java8;

import java.util.Comparator;

import com.sapient.haider.entity.Person;

public class ComparatorExample {
	public static Comparator<Person> byFirstName = new Comparator<Person>() {
		@Override
		public int compare(Person o1, Person o2) {
			return o1.getFirstName().compareTo(o2.getFirstName());
		}
	};
	// From java 8
	public static Comparator<Person> byLastName = (Person o1, Person o2) -> o1.getLastName()
			.compareTo(o2.getLastName());
}
