package com.sapient.haider.wtn;
/**
 * @author haiali1
 * Class must be final - so no one can extend it
 *  Make all fields final and private so can be initialized only once
 * Constructor must have all fields as arguments
 * Getter for mutable objects or any other method returning mutable object which is part of class must always return a clone
 * Don’t provide “setter” methods — methods that modify fields or objects referred to by fields.
 * Immutable objects are automatically thread-safe, the overhead caused due to use of synchronization is avoided.
 * Once created the state of the immutable object can not be changed so there is no possibility of them getting into an inconsistent state.
 * The references to the immutable objects can be easily shared or cached without having to copy or clone them as there state can not be changed ever after construction.
 * The best use of the immutable objects is as the keys of a map.
 */
public final class StudentImmutable {
	private final int id;
	private final String name;
	private final Age age;
	public StudentImmutable(int id, String name, Age age) {
		super();
		this.id = id;
		this.name = name;
		this.age = (Age)age.clone();
	}
	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public Age getAge() {
		return (Age)age.clone();
	}
	
	public static void main(String[] args) {
		Age age = new Age(1,1,1992);
	    StudentImmutable student = new StudentImmutable(1, "Alex", age);
	    System.out.println("Alex age year before modification = " + student.getAge().getYear());
	    student.getAge().setYear(1993);
	    System.out.println("Alex age year after modification = " + student.getAge().getYear());
	}
}
