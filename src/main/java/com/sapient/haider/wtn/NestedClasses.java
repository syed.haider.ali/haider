package com.sapient.haider.wtn;

/**
 * @author haiali1 
 * There are two types of nested classes non-static and static nested classes
 * The non-static nested classes are also known as inner classes 
 * Inner class Types : Member inner class, Anonymous inner class, Local inner class.
 * The java compiler creates two class files in case of inner member class.
 * The class file name of inner class is "<OuterClassName>$<InnerClassame>.class [NestedClasses$MemberInner.class]"
 * If you want to instantiate inner class, you must have to create the instance of outer class
 * A class that have no name is known as anonymous inner class in java
 * It should be used if you have to override method of class or interface
 * The class file name of anonymous class is "<OuterClassName>$<index>.class [NestedClasses$1.class]"
 * Anonymous class extends class or implements interface from which you have created it.
 *  A class i.e. created inside a method is called local inner class in java.
 *  If you want to invoke the methods of local inner class, you must instantiate this class inside the method.
 *  The class file name of anonymous class is "<OuterClassName>$<index><InnerClassname>.class [NestedClasses$1LocalInner.class]"
 */

public class NestedClasses {
	private int data=30;
	
	class MemberInner {
		void msg() {
			System.out.println("data is " + data);
		}
	}
	
	 void display(){  
		  class LocalInner{  
		   void msg(){System.out.println(data);}  
		  }  
		  LocalInner l=new LocalInner();  
		  l.msg();  
		 } 

	public static void main(String[] args) {
		//Member Class
		NestedClasses nestedClasses = new NestedClasses();
		MemberInner memberInner=nestedClasses.new MemberInner();
		memberInner.msg();
		//Anonymous class
		HalfClass halfClass= new HalfClass() {
			
			@Override
			void method() {
				System.out.println("This will run");
				
			}
		};
		halfClass.method();
		nestedClasses.display();
	}
}

abstract class HalfClass{
	abstract void method();
}
