package com.sapient.haider.wtn;

public class Age implements Cloneable {
	private int date;
	private int month;
	private int year;
	public Age(int date, int month, int year) {
		super();
		this.date = date;
		this.month = month;
		this.year = year;
	}
	public int getDate() {
		return date;
	}
	public void setDate(int date) {
		this.date = date;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	@Override
	protected Object clone(){
		return new Age(this.date, this.month, this.year);
	}
	
}
